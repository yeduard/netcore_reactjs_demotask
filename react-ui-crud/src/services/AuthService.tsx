import { start } from "node:repl";
import { LOGIN_API_URL } from "../constants";

export const AuthService = {
    ValidateJWT,
    Login,
};

function Login(email: string, password: string): Promise<boolean> {
    console.log(process.env);
    return fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: email,
            password: password
        })
    })
    .then(res => {
        if (res.status == 401) {
            return;
        }

        return res.json();
    })
    .then(data => {
        SetJwt(data);
        return true;
    })
    .catch(error => {
        return false;
    });
}

function SetJwt(data: any) {
    window.localStorage.setItem('jwt', data.token);
    window.localStorage.setItem('expires', JSON.stringify(data.expires));
}

function LogUserOut(): void {
    localStorage.removeItem('expires');
    localStorage.removeItem('jwt');
}

function ValidateJWT(): boolean {
    var expireTime = window.localStorage.getItem('expires');
    var jwt = window.localStorage.getItem('jwt');

    if (!jwt || !expireTime || SessionExpired(expireTime)) {
        LogUserOut();
        return false;
    }

    return true;
}

function SessionExpired(expireTime: string): boolean {
    if (!expireTime) {
        return false;
    }

    let expireDate = new Date(parseInt(expireTime));

    return expireDate <= new Date();
}