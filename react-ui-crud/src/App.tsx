import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'

import Routes from './routes/routes';
import Login from './components/login/login';

import Landing from './components/Landing';

import './App.css';

class App extends Component {

  render() {
    return (
      <Router basename={process.env.PUBLIC_URL}>
        <div className="App">
          <Switch>
            <Route exact path="/" render={() => (<Redirect to="/Home" />)}></Route>
            <Route exact path="/Login" component={Login}></Route>
            <Routes exact path="/Home" component={Landing} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
