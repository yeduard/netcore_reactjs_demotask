import React, { Component } from 'react';
import { Col, Container, Row } from 'reactstrap';
import DataTable from './DataTable';
import RegistrationModal from './form/RegistrationModal';

class Home extends Component {

  state = {
    items: []
  };

  jwtToken = window.localStorage.getItem('jwt');

  componentDidMount() {
    this.getItens();
  }

  getItens = () => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users`, {
      method: 'get',
      headers: new Headers({
        'Authorization': `Bearer ${this.jwtToken}`
      })
    })
      .then(res => res.json())
      .then(res => this.setState({ items: res }))
      .catch(err => console.log(err));
  }

  addUserToState = user => {
    this.setState(previous => ({
      items: [...previous.items, user]
    }));
  }

  updateState = (id) => {
    this.getItens();
  }

  deleteItemFromState = id => {
    const updated = this.state.items.filter(item => item.userId !== id);
    this.setState({ items: updated })
  }

  render() {
    return <Container style={{ paddingTop: "100px" }}>
      <Row>
        <Col>
          <h3>Kambda .Net Core + ReactJS Demo Task</h3>
        </Col>
      </Row>
      <Row>
        <Col>
          <DataTable
            items={this.state.items}
            updateState={this.updateState}
            deleteItemFromState={this.deleteItemFromState} />
        </Col>
      </Row>
      <Row>
        <Col>
          <RegistrationModal isNew={true} addUserToState={this.addUserToState} />
        </Col>
      </Row>
    </Container>;
  }
}

export default Home;
