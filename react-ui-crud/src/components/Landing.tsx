import React, { Fragment, Component } from 'react';

import AppHeader from './AppHeader';
import Home from './Home';
import AppFooter from './AppFooter';

interface IProps {

}

interface IState {
    isEmptyState: boolean;
    currentComponent: JSX.Element;
    componentName: string;
    isLoading: boolean;
    response: any;
    errorState: boolean;
    loadingText: string;
}

class Landing extends Component<IProps, IState> {
    constructor(props: any) {
        super(props);
    }

    render() {
        return  (
            <Fragment>
                <AppHeader />
                <Home />
                <AppFooter />
            </Fragment>
        );  
    }
}

export default Landing;