import { Form, Input, Button, Row, Spin, message } from 'antd';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { AuthService } from '../../services/AuthService';

import 'antd/dist/antd.css'

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };
  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  };

const displayError = (error: any) => {
    message.error(error, 5);
};

const displaySuccess = (success: any) => {
    message.success(success, 5);
};

const Login = () => {
    const [isLoading, setIsLoading] = useState(false);

    let history = useHistory();

    const Login = (values: any) => {
        setIsLoading(true);

        AuthService.Login(values.email, values.password).then(res =>{
            if (res)
            {
                setIsLoading(false);
                history.push('/');
                displaySuccess('Welcome back!');
                return;
            }

            setIsLoading(false);
            displayError('Invalid email or passowrd');
        });
    }

    return (
        <div className='Login'>
            <Spin spinning={isLoading} size="large" tip="Logging in...">
                <Row justify="center" align="middle" style={{paddingLeft: '200px', paddingBottom: '80px', paddingTop: '80px'}}>
                    <div> 
                        {/* <img src={logo} alt="Logo" />  */}
                        <img src="https://cdn-alnbj.nitrocdn.com/UABFRiSYcuFNUizeGciWowjnvkBpwQwl/assets/static/source/rev-0eb185a/wp-content/uploads/2020/04/Logo-kambda-white.svg" 
                        width="300"
                        className="d-inline-block align-top" alt="" />
                    </div>
                </Row>

                <Row justify="center" align="middle" style={{minHeight: '30vh'}}>
                    <div style={{height:'300px', justifyContent:"center", alignContent:'middle', width:'600px', alignSelf:'middle', border:'3px solid rgb(212,212, 212)', borderRadius:'10px'}}>
                        <Form className="loginForm" style={{ paddingTop: '50px', paddingRight: '80px' }}
                            {...layout}
                            name="basic"
                            // initialValues={{ remember: true }}
                            onFinish={Login}
                        >
                            
                            <Form.Item label='Email' name='email' initialValue='root@root.com' rules={[{ required: true, message: 'Please input your email' }]}>
                                <Input />
                            </Form.Item>

                            <Form.Item label='Password' name='password' initialValue='root' rules={[{ required: true, message: 'Please input your password' }]}>
                                <Input.Password />
                            </Form.Item>

                            <Form.Item {...tailLayout}>
                                <Button id='submit_login' type='primary' htmlType='submit'>
                                    Submit
                                </Button>
                            </Form.Item>
                        
                        </Form>
                    </div>
                </Row>
            </Spin>
        </div>
    )
};

export default Login;