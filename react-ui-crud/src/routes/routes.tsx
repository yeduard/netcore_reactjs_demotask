import * as React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { AuthService } from './../services/AuthService';

const Routes: React.ComponentType<any> = ({
    component: Component,
    ...rest
}) => {
    return (
        <Route
            {...rest}
            render = {props =>
                AuthService.ValidateJWT() ? <Component {...props} /> : ( <Redirect to={{ pathname: "/login", state: { from: props.location } }} /> ) }
        />
    );
};

export default Routes;