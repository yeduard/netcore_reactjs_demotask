# Kambda ReactASPNET Demo Task

This is a Demo application for Kambda.

Developed by Jeison Rojas H

## Run the application
In order to run the following application please execute the following commands.

Move to the application folder
``` cmd
cd netcore_reactjs_demotask 
```

In order to build the application, execute this command
``` cmd
docker-compose build
```

This will generate 3 docker images
- datbase (SQL Server)
- backend (dotnet API)
- frontend (react UI)

Notice that in order to run the database, you need to stop your local SQL Server instance, because docker needs the port `1433` free in order to be used.

Now that we have all the dependencies ready, we can run our application.
``` cmd
cd docker-compose up
```

## Images URL

The 3 docker images have 3 different URLs
You can access the application by looking at this addresses in th browser

### Backend
[Backend Swagger](http://localhost:5000/index.html)

### Frontend
[React UI](http://localhost:3000/login)

## Frontend

For your benefit, the login credentials are set up in the UI project.
But here is the credentials for the `root` user.
| Email | Password |
| :---------: | :---------: |
| `root@root.com` | `root` |

## Users credentials
For all the created users, this is the default password
| Email | Password |
| :---------: | :---------: |
| `{userEmail}` | `Intlog6151` |

## Delete images
In order to delete this images you can execute this command
```cmd
docker-compose rm
```