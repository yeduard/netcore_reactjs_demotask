﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ReactASPCrud.Views
{
    public class UserView
    {
        public long UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Document { get; set; }
        public string Phone { get; set; }

        public long CreatedAt { get; set; }
        public long? UpdatedAt { get; set; }
        public long? DeletedAt { get; set; }
    }

    public class CreateUserView
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Document { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }
    }

    public class LoginUserView
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
