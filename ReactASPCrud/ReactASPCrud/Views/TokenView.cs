﻿namespace ReactASPCrud.Views
{
    public class TokenView
    {
        public string Token { get; set; }
        public long Expires { get; set; }
        public string Name { get; set; }
    }
}
