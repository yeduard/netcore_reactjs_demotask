﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ReactASPCrud.Helpers;

namespace ReactASPCrud.Models
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long UserId { get; protected set; }

        public string Name { get; protected set; }
        public string Email { get; protected set; }
        public string Document { get; protected set; }
        public string Phone { get; protected set; }

        public string Password { get; protected set; }
        public string Salt { get; protected set; }

        protected User() { }

        public User(string name, string email, string document, string phone)
        {
            this.Name = name;
            this.Email = email;
            this.Document = document;
            this.Phone = phone;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreatedAt { get; protected set; } = DateTime.UtcNow;
        public DateTime? ModifiedAt { get; protected set; } = DateTime.UtcNow;
        public DateTime? DeletedAt { get; protected set; }

        public void SetPassword(string password, IEncrypter encrypter)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new Exception($"User password cannot be empty.");
            }

            Salt = encrypter.GetSalt(password);
            Password = encrypter.GetHash(password, Salt);
        }

        public bool ValidatePassword(string password, IEncrypter encrypter)
            => Password.Equals(encrypter.GetHash(password, Salt));

        public void UpdateUserId(long userId) => this.UserId = userId;

        public void Update(string name, string email, string document, string phone)
        {
            this.Name = name;
            this.Email = email;
            this.Document = document;
            this.Phone = phone;

            this.ModifiedAt = DateTime.UtcNow;
        }

        public void Delete() => this.DeletedAt = DateTime.UtcNow;
    }
}
