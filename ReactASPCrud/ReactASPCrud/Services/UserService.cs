using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ReactASPCrud.Helpers;
using ReactASPCrud.Models;
using ReactASPCrud.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactASPCrud.Services
{
    public class UserService : IUserService
    {
        private readonly string _defaultPassword = "Intlog6151";
        private readonly string _rootUserEmail = "root@root.com";
        private readonly DatabaseContext _database;
        private readonly ILogger<UserService> _logger;
        private readonly IEncrypter _encrypter;

        public UserService(DatabaseContext database, ILogger<UserService> logger, IEncrypter encrypter)
        {
            this._database = database;
            this._logger = logger;
            this._encrypter = encrypter;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            _logger.LogInformation($"Getting all users in database");
            return await _database.Users.Where(user => user.Email != _rootUserEmail && user.DeletedAt == null).ToListAsync();
        }

        public async Task<User> GetById(long userId)
        {
            _logger.LogInformation($"Getting user by userId {userId} in database");
            return await _database.Users.FirstOrDefaultAsync(user => user.Email != _rootUserEmail && user.UserId == userId && user.DeletedAt == null);
        }

        public async Task<long> CreateUser(CreateUserView userView)
        {
            _logger.LogInformation($"Creating new user {userView.Name} in database");

            User newUser = new User(userView.Name, userView.Email, userView.Document, userView.Phone);
            newUser.SetPassword(_defaultPassword, _encrypter);

            await _database.Users.AddAsync(newUser);
            await _database.SaveChangesAsync();

            return newUser.UserId;
        }

        public async Task UpdateUser(long userId, CreateUserView userView)
        {
            _logger.LogInformation($"Updating userId {userId} in database");
            var user = await _database.Users.FirstOrDefaultAsync(user => user.Email != _rootUserEmail && user.UserId == userId && user.DeletedAt == null);

            if (user == null)
                return;

            user.Update(userView.Name, userView.Email, userView.Document, userView.Phone);
            await _database.SaveChangesAsync();
        }

        public async Task DeleteUser(long userId)
        {
            _logger.LogInformation($"Deleting userId {userId} in database");
            var user = await _database.Users.FirstOrDefaultAsync(user => user.Email != _rootUserEmail && user.UserId == userId && user.DeletedAt == null);

            if (user == null)
                return;

            user.Delete();

            await _database.SaveChangesAsync();
        }

        public async Task<User> LoginUser(LoginUserView userView)
        {
            _logger.LogInformation($"Getting user by email {userView.Email} in database");
            var user = await _database.Users.FirstOrDefaultAsync(user => user.Email == userView.Email && user.DeletedAt == null);

            if (user != null && user.ValidatePassword(userView.Password, _encrypter))
                return user;

            return null;
        }
    }
}
