﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ReactASPCrud.Models;
using ReactASPCrud.Views;

namespace ReactASPCrud.Services
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetAll();
        Task<User> GetById(long userId);

        Task<long> CreateUser(CreateUserView userView);
        Task UpdateUser(long userId, CreateUserView userView);
        Task DeleteUser(long userId);

        Task<User> LoginUser(LoginUserView userView);
    }
}
