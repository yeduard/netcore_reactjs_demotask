﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReactASPCrud.Authentication;
using ReactASPCrud.Factories;
using ReactASPCrud.Services;
using ReactASPCrud.Views;

namespace ReactASPCrud.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILogger<UsersController> _logger;
        private readonly IJwtHandler _jwtHandler;

        public UsersController(IUserService userService, ILogger<UsersController> logger, IJwtHandler jwtHandler)
        {
            this._userService = userService;
            this._logger = logger;
            this._jwtHandler = jwtHandler;
        }

        // GET api/users
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Get()
        {
            _logger.LogInformation($"Looking for all users");
            var users = await _userService.GetAll();

            return Ok(users.Select(UserFactory.GenerateUser));
        }

        // GET api/users/5
        [HttpGet("{userId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Get(long userId)
        {
            _logger.LogInformation($"Looking for user id {userId}");
            var user = await _userService.GetById(userId);

            if (user == null)
                return NotFound();

            return Ok(UserFactory.GenerateUser(user));
        }

        // POST api/users
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Post(CreateUserView userView)
        {
            _logger.LogInformation($"Creating a new user");
            long userId = await _userService.CreateUser(userView);

            return CreatedAtAction("Get", new { id = userId });
        }

        // PUT api/users/5
        [HttpPut("{userId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Put(long userId, CreateUserView userView)
        {
            _logger.LogInformation($"Updating user id {userId}");
            await _userService.UpdateUser(userId, userView);

            return NoContent();
        }

        // DELETE api/users/5
        [HttpDelete("{userId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete(long userId)
        {
            _logger.LogInformation($"Deliting user id {userId}");
            await _userService.DeleteUser(userId);

            return NoContent();
        }

        // POST api/users/login
        [HttpPost, Route("login")]
        public async Task<IActionResult> Login(LoginUserView userView)
        {
            _logger.LogInformation($"Login user {userView.Email}");
            var user = await _userService.LoginUser(userView);

            if (user == null)
                return Unauthorized();

            return Ok(_jwtHandler.Create(user));
        }

        [HttpGet, Route("verify")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Verify()
        {
            _logger.LogDebug($"Verifing token: {User.Identity.Name}");
            var user = await _userService.GetById(Convert.ToInt64(User.Identity.Name));

            if (user == null)
                return Unauthorized();

            return Ok(UserFactory.GenerateUser(user));
        }
    }
}
