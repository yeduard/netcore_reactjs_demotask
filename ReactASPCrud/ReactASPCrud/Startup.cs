﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using ReactASPCrud.Authentication;
using ReactASPCrud.Helpers;
using ReactASPCrud.Models;
using ReactASPCrud.Services;

namespace ReactASPCrud
{
    public class Startup
    {
        public Startup(IHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }
        public ILogger<Startup> _logger;

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc(options => options.EnableEndpointRouting = false).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddJwt(Configuration);

            services.AddScoped<IEncrypter, Encrypter>();

            services.AddDbContext<DatabaseContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Develop"))
            );

            services.AddTransient<IUserService, UserService>();

            services.AddSwaggerGen(opt =>
            {
                opt.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "Kambda API",
                        Version = "v1",
                    });
            });

            services.AddCors(opt => {
                opt.AddPolicy(MyAllowSpecificOrigins,
                builder => {
                    builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger, IEncrypter encrypter)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            _logger = logger;

            app.UseSwagger();
            app.UseSwaggerUI(s =>
            {
                s.RoutePrefix = "";
                s.SwaggerEndpoint("/swagger/v1/swagger.json", "Kambda API V1");
            });

            app.UseCors(MyAllowSpecificOrigins);
            app.UseHttpsRedirection();

            app.UseRouting();

            //app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            UpdateDatabase(app, encrypter);
        }

        private void UpdateDatabase(IApplicationBuilder app, IEncrypter encrypter)
        {
            _logger.LogDebug("********************");
            _logger.LogDebug("Validating if database have pending migrations.");

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                using (DatabaseContext context = serviceScope.ServiceProvider.GetService<DatabaseContext>())
                {
                    try
                    {
                        var migrations = context.Database.GetPendingMigrations();

                        _logger.LogDebug($"The database have {migrations.Count()} pending migrations.");

                        if (migrations.Count() > 0)
                        {
                            _logger.LogDebug($"Aplying {migrations.Count()} pending migrations to database.");
                            context.Database.Migrate();
                        }

                        _logger.LogDebug("**********************************************");
                        _logger.LogDebug("Verifying if we should create a new database");

                        bool databaseCreated = context.Database.EnsureCreated();

                        _logger.LogDebug($"{databaseCreated}");

                        if (databaseCreated)
                        {
                            _logger.LogDebug($"The database was succesfully added.");
                        }

                        _logger.LogDebug("**********************************************");
                        _logger.LogDebug("Verifying if root user is initiated");

                        if (context.Users.Count(x => x.Email == "root@root.com") == 0)
                        {
                            var rootUser = new User("Root Rootie", "root@root.com", "604310338", "84857107");
                            rootUser.SetPassword("root", encrypter);

                            context.Users.Add(rootUser);
                            context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError("**********************************************");
                        _logger.LogError("A new exception has ocurred");
                        _logger.LogError($"{ex.ToString()}");
                    }
                }
            }
        }
    }
}
