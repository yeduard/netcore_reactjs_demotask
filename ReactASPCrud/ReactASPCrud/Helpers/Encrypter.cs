﻿using System;
using System.Security.Cryptography;

namespace ReactASPCrud.Helpers
{
    public class Encrypter : IEncrypter
    {
        private static readonly int Saltsize = 40;
        private static readonly int DeriveBytesIteractionCount = 10000;

        public string GetSalt(string value)
        {
            var random = new Random();
            var saltBytes = new byte[Saltsize];
            var rng = RandomNumberGenerator.Create();
            rng.GetBytes(saltBytes);

            return Convert.ToBase64String(saltBytes);
        }

        public string GetHash(string value, string salt)
        {
            var pbkdf2 = new Rfc2898DeriveBytes(value, GetBytes(salt), DeriveBytesIteractionCount);

            return Convert.ToBase64String(pbkdf2.GetBytes(Saltsize));
        }

        private static byte[] GetBytes(string value)
        {
            var bytes = new byte[value.Length * sizeof(char)];
            Buffer.BlockCopy(value.ToCharArray(), 0, bytes, 0, bytes.Length);

            return bytes;
        }
    }
}
