﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ReactASPCrud.Models;
using ReactASPCrud.Views;

namespace ReactASPCrud.Authentication
{
    public class JwtHandler : IJwtHandler
    {
        private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler = new JwtSecurityTokenHandler();

        private readonly JwtOptions _options;
        private readonly SecurityKey _issuerSigningKey;
        private readonly SigningCredentials _signingCredentials;

        public JwtHandler(IOptions<JwtOptions> options)
        {
            _options = options.Value;
            _issuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_options.SecretKey));
            _signingCredentials = new SigningCredentials(_issuerSigningKey, SecurityAlgorithms.HmacSha256);
        }

        public TokenView Create(User user)
        {
            var nowUtc = DateTime.UtcNow;

            //Create Token
            var expires = nowUtc.AddMinutes(_options.ExpireMinutes);
            var exp = new DateTimeOffset(expires).ToUnixTimeMilliseconds();

            var payload = new SecurityTokenDescriptor
            {
                Issuer = _options.Issuer,
                IssuedAt = nowUtc,
                Expires = expires,
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(ClaimTypes.Name, user.UserId.ToString())
                }),
                SigningCredentials = _signingCredentials
            };
            var token = _jwtSecurityTokenHandler.CreateToken(payload);

            return new TokenView
            {
                Token = _jwtSecurityTokenHandler.WriteToken(token),
                Expires = exp,
                Name = user.Name
            };
        }
    }
}
