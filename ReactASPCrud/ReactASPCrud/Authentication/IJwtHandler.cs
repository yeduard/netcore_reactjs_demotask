﻿using System;
using ReactASPCrud.Models;
using ReactASPCrud.Views;

namespace ReactASPCrud.Authentication
{
    public interface IJwtHandler
    {
        TokenView Create(User user);
    }
}
