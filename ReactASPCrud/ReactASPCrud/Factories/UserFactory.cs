﻿using System;
using ReactASPCrud.Models;
using ReactASPCrud.Views;

namespace ReactASPCrud.Factories
{
    public static class UserFactory
    {
        public static UserView GenerateUser(User user) => new UserView
        {
            UserId = user.UserId,
            Name = user.Name,
            Email = user.Email,
            Document = user.Document,
            Phone = user.Phone,
            CreatedAt = ((DateTimeOffset)user.CreatedAt).ToUnixTimeMilliseconds(),
            UpdatedAt = user.ModifiedAt.HasValue ? ((DateTimeOffset)user.ModifiedAt).ToUnixTimeMilliseconds() : 0,
            DeletedAt = user.DeletedAt.HasValue ? ((DateTimeOffset)user.DeletedAt).ToUnixTimeMilliseconds() : 0
        };
    }
}
